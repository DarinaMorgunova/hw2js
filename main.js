// 1. Null: нульове або пусте значення.
// Undefined:  змінна, якій не було призначено значення.
// Number: числове значення, як цілі числа, так і числа з комою.
// Boolean: логічне значення true або false.
// String: використовується для рядків.
// Symbol: унікальний ідентифікатор, який використовується для уникнення конфліктів при іменуванні.
// Object: представляє собою набір пар ключ-значення.
// 2. Основна відмінність між ними полягає в тому, як вони виконують порівняння. 
// 3.  Це символ , яке виконує операцію над одним або декількома даними.


let userName = prompt("What's your name?");
let userAge = prompt("How old are you?");
while (!userName | isNaN(userAge)) {
    userName = prompt("Please enter your name.");
    userAge = prompt("Please enter your age.");
  }
  
  if (userAge < 18) {
    alert("You are not allowed to visit this website.");
  } 
  else if (userAge >= 18 && userAge <= 22) {
    const result = confirm("Are you sure you want to continue?");
    if (result) {
      alert(`Welcome, ${userName}!`);
    } 
    else {
      alert("You are not allowed to visit this website.");
    }
  } 
  else {
    alert(`Welcome, ${userName}!`);
  }
  